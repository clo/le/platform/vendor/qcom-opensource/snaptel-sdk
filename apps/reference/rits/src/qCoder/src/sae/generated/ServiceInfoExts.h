/*
 * Generated by asn1c-0.9.29 (http://lionet.info/asn1c)
 * From ASN.1 module "IEEE-1609-3-WSA"
 * 	found in "../Ieee1609Dot3Wsa.asn"
 * 	`asn1c -fcompound-names -gen-PER`
 */

#ifndef	_ServiceInfoExts_H_
#define	_ServiceInfoExts_H_


#include <asn_application.h>

/* Including external dependencies */
#include <asn_SEQUENCE_OF.h>
#include <constr_SEQUENCE_OF.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Forward declarations */
struct Extension;

/* ServiceInfoExts */
typedef struct ServiceInfoExts {
	A_SEQUENCE_OF(struct Extension) list;
	
	/* Context for parsing across buffer boundaries */
	asn_struct_ctx_t _asn_ctx;
} ServiceInfoExts_t;

/* Implementation */
extern asn_TYPE_descriptor_t asn_DEF_ServiceInfoExts;

#ifdef __cplusplus
}
#endif

/* Referred external types */
#include "Extension.h"

#endif	/* _ServiceInfoExts_H_ */
#include <asn_internal.h>
