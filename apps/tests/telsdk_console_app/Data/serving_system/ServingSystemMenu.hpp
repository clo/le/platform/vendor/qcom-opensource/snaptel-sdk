/*
 *  Copyright (c) 2020-2021, The Linux Foundation. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 *  ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 *  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * This is a Serving System Manager Sample Application using Telematics SDK.
 * It is used to demonstrate API to exercise Serving System Features.
 */

#ifndef DATASERVINGSYSTEMMENU_HPP
#define DATASERVINGSYSTEMMENU_HPP

#include <algorithm>
#include <iostream>
#include <memory>
#include <string>
#include <iomanip>
#include <telux/common/CommonDefines.hpp>
#include <telux/data/DataDefines.hpp>
#include <telux/data/DataFactory.hpp>
#include "console_app_framework/ConsoleApp.hpp"
#include "ServingSystemListener.hpp"

using namespace telux::data;
using namespace telux::common;

class DataServingSystemMenu : public ConsoleApp,
                              public telux::data::IServingSystemListener,
                              public std::enable_shared_from_this<DataServingSystemMenu> {
public:
    // initialize menu and sdk
    bool init();
    // Menu Functions

    DataServingSystemMenu(std::string appName, std::string cursor);

    //Initialization Callback
    void onInitCompleted(telux::common::ServiceStatus status);

    //API
    void getDrbStatus(std::vector<std::string> inputCommand);
    void requestServiceStatus(std::vector<std::string> inputCommand);
    void requestRoamingStatus(std::vector<std::string> inputCommand);

    ~DataServingSystemMenu();
private:
    bool addMenuCmds_;
    bool subSystemStatusUpdated_;
    std::mutex mtx_;
    std::condition_variable cv_;
    std::map<SlotId, std::shared_ptr<IServingSystemManager>> dataServingSystemManagers_;
    std::map<SlotId, std::shared_ptr<IServingSystemListener>> dataServingSystemListeners_;
    bool initServingSystemManagerAndListener(SlotId slotId);
};

#endif
