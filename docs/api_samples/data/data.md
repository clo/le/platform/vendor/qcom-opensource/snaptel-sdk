data {#data}
================

# The List of sample apps related to data:

* @subpage start_or_stop_data_call
* @subpage request_profile_list
* @subpage get_and_set_data_filter_mode
* @subpage add_data_filter
* @subpage remove_data_filter
* @subpage enable_disable_firewall
* @subpage create_firewall_dmz
* @subpage add_firewall
* @subpage add_enable_software_bridge
* @subpage remove_disable_software_bridge
* @subpage create_and_bind_vlan
* @subpage create_snat_entry
* @subpage enable_and_add_tunnel_l2tp
* @subpage enable_disable_socks
* @subpage get_drb_status_and_indication
* @subpage get_service_status_and_indication
* @subpage get_roaming_status_and_indication
