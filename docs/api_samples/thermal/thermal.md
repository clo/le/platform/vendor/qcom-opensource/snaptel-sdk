thermal {#thermal}
============

# The List of sample apps related to thermal:

* @subpage thermal_manager
* @subpage get_thermal_autoshutdown_mode_updates
* @subpage send_thermal_shutdown_mode_commands
