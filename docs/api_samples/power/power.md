power {#power}
============

# The List of sample apps related to power:

* @subpage get_tcu_activity_state_notifications
* @subpage set_tcu_activity_state_active_mode
* @subpage set_tcu_activity_state_passive_mode
