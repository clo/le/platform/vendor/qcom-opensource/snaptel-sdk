/*
 * Generated by asn1c-0.9.29 (http://lionet.info/asn1c)
 * From ASN.1 module "IEEE-1609-3-WSA"
 * 	found in "../Ieee1609Dot3Wsa.asn"
 * 	`asn1c -fcompound-names -gen-PER`
 */

#ifndef	_RsvAdvPrtVersion_H_
#define	_RsvAdvPrtVersion_H_


#include <asn_application.h>

/* Including external dependencies */
#include <NativeInteger.h>

#ifdef __cplusplus
extern "C" {
#endif

/* RsvAdvPrtVersion */
typedef long	 RsvAdvPrtVersion_t;

/* Implementation */
extern asn_TYPE_descriptor_t asn_DEF_RsvAdvPrtVersion;
asn_struct_free_f RsvAdvPrtVersion_free;
asn_struct_print_f RsvAdvPrtVersion_print;
asn_constr_check_f RsvAdvPrtVersion_constraint;
ber_type_decoder_f RsvAdvPrtVersion_decode_ber;
der_type_encoder_f RsvAdvPrtVersion_encode_der;
xer_type_decoder_f RsvAdvPrtVersion_decode_xer;
xer_type_encoder_f RsvAdvPrtVersion_encode_xer;
per_type_decoder_f RsvAdvPrtVersion_decode_uper;
per_type_encoder_f RsvAdvPrtVersion_encode_uper;

#ifdef __cplusplus
}
#endif

#endif	/* _RsvAdvPrtVersion_H_ */
#include <asn_internal.h>
