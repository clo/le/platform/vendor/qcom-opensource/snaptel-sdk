cmake_minimum_required(VERSION 2.8.9)

set(TARGET_DATA_FILTER_APP data_filter_app)

set(SOURCES
    DataFilterApp.cpp
)

# set global variables
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

add_executable (${TARGET_DATA_FILTER_APP} ${SOURCES})
target_link_libraries(${TARGET_DATA_FILTER_APP} telux_data)

# install to target
install ( TARGETS ${TARGET_DATA_FILTER_APP}
          RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
          ARCHIVE DESTINATION ${CMAKE_INSTALL_BINDIR} )
