/*
 *  Copyright (c) 2017, 2021 The Linux Foundation. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 *  ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 *  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <vector>
#include <string>
#include <memory>

#include <telux/tel/PhoneListener.hpp>
#include <telux/tel/SmsManager.hpp>
#include <telux/tel/PhoneFactory.hpp>

#define PRINT_NOTIFICATION std::cout << "\033[1;35mNOTIFICATION: \033[0m"

using namespace telux::tel;

// [1] Implement ISmsListener interface to receive incoming SMS
class MySmsListener : public ISmsListener {
public:
   void onIncomingSms(int phoneId, std::shared_ptr<SmsMessage> message) override;
};

void MySmsListener::onIncomingSms(int phoneId, std::shared_ptr<SmsMessage> smsMsg) {
   PRINT_NOTIFICATION << "MySmsListener::onIncomingSms from PhoneId : " << phoneId << std::endl;
   PRINT_NOTIFICATION << "smsReceived: " << smsMsg->toString() << std::endl;
}

/**
 * Main routine
 */
int main(int, char **) {

    //Instantiate initialization status callback
    std::promise<telux::common::ServiceStatus> initCallbackPromise;
    auto initCb = [&](telux::common::ServiceStatus status) {
      initCallbackPromise.set_value(status);
    };
    // [1] Get the PhoneFactory and SMS Manager instance for appropriate phoneId.
    auto &phoneFactory = telux::tel::PhoneFactory::getInstance();
    auto smsManager = phoneFactory.getSmsManager(DEFAULT_SLOT_ID, initCb);
    if(!smsManager) {
       std::cout << "Failed to get SMS Manager instance" << std::endl;
       return 1;
    }

    // [2] Wait for SMS subsystem to be ready
    std::cout << "Waiting for SMS Manager to be ready" << std::endl;
    telux::common::ServiceStatus subSystemsStatus = initCallbackPromise.get_future().get();
    if(subSystemsStatus != telux::common::ServiceStatus::SERVICE_AVAILABLE) {
      std::cout << " *** ERROR - SMS Manager initialization failed" << std::endl;
      return 1;
    }

   // [3] Instantiate global ISmsListener
   auto mySmsListener = std::make_shared<MySmsListener>();

   // [4] Register of for incoming SMS messages
   if(smsManager) {
      smsManager->registerListener(mySmsListener);
   }

   // [5] wait for the onIncomingSms()
   std::cout << " *** wait for the onIncomingSms() *** " << std::endl;
   std::cout << "Press enter to exit" << std::endl;
   std::string input;
   std::getline(std::cin, input);
   std::cout << " Exiting application... " << std::endl;
   return 0;
}
