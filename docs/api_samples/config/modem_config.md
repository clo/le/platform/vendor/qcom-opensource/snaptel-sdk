config {#modem_config}
================

# The List of sample apps related to modem config:

* @subpage load_and_activate_modem_config_file
* @subpage get_and_set_auto_selection_mode
* @subpage deactivate_and_delete_modem_config_file
