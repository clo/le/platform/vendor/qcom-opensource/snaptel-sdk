tel {#phone}
================

# The List of sample apps related to telephony:

* @subpage make_call
* @subpage make_eCall
* @subpage request_voice_service_state
* @subpage set_radio_power
* @subpage serving_system
* @subpage get_subscription
* @subpage card_services_app
* @subpage sap_api_and_listener
* @subpage network_selection
* @subpage remote_sim_api
* @subpage remote_sim_reference_apps
* @subpage send_sms
* @subpage listen_sms
* @subpage rsp
