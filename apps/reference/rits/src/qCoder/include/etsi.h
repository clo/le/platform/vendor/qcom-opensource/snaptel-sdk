/*
 * Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef __ETSI_H__
#define __ETSI_H__

#ifdef __cplusplus
extern "C" {
#endif
#include "v2x_msg.h"
/**
 * decode_as_etsi Decode etsi message, CAM or DENM.
 *
 * @param [in] mc the message content, mc->abuf contains the buffer to be
 * decoded.
 *
 * @return 0 on success or -1 on failure.
 *
 */
int decode_as_etsi(msg_contents *mc);
/**
 * encode the etsi messages, CAM/DENM so far.
 * @param mc the message content, in which the mc->etsi_msg_id specify what
 * message to encode.
 * @return encoded length on success or -1 on failure.
 */
int encode_as_etsi(msg_contents *mc);
/**
 * print_cam print the decoded CAM message.
 *
 * @param [in] cam the decoded cam data structure.
 * @return none.
 */
void print_cam(void *cam);
/**
 * print_denm print the decoded CAM message.
 *
 * @param [in] denm the decoded DENM data structure.
 * @return none.
 */
void print_denm(void *denm);
#ifdef __cplusplus
}
#endif
#endif
