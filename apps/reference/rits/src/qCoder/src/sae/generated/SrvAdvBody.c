/*
 * Generated by asn1c-0.9.29 (http://lionet.info/asn1c)
 * From ASN.1 module "IEEE-1609-3-WSA"
 * 	found in "../Ieee1609Dot3Wsa.asn"
 * 	`asn1c -fcompound-names -gen-PER`
 */

#include "SrvAdvBody.h"

static asn_TYPE_member_t asn_MBR_SrvAdvBody_1[] = {
	{ ATF_NOFLAGS, 0, offsetof(struct SrvAdvBody, changeCount),
		(ASN_TAG_CLASS_CONTEXT | (0 << 2)),
		-1,	/* IMPLICIT tag at current level */
		&asn_DEF_SrvAdvChangeCount,
		0,	/* Defer constraints checking to the member type */
		0,	/* No PER visible constraints */
		0,
		"changeCount"
		},
	{ ATF_POINTER, 4, offsetof(struct SrvAdvBody, extensions),
		(ASN_TAG_CLASS_CONTEXT | (1 << 2)),
		-1,	/* IMPLICIT tag at current level */
		&asn_DEF_SrvAdvMsgHeaderExts,
		0,	/* Defer constraints checking to the member type */
		0,	/* No PER visible constraints */
		0,
		"extensions"
		},
	{ ATF_POINTER, 3, offsetof(struct SrvAdvBody, serviceInfos),
		(ASN_TAG_CLASS_CONTEXT | (2 << 2)),
		-1,	/* IMPLICIT tag at current level */
		&asn_DEF_ServiceInfos,
		0,	/* Defer constraints checking to the member type */
		0,	/* No PER visible constraints */
		0,
		"serviceInfos"
		},
	{ ATF_POINTER, 2, offsetof(struct SrvAdvBody, channelInfos),
		(ASN_TAG_CLASS_CONTEXT | (3 << 2)),
		-1,	/* IMPLICIT tag at current level */
		&asn_DEF_ChannelInfos,
		0,	/* Defer constraints checking to the member type */
		0,	/* No PER visible constraints */
		0,
		"channelInfos"
		},
	{ ATF_POINTER, 1, offsetof(struct SrvAdvBody, routingAdvertisement),
		(ASN_TAG_CLASS_CONTEXT | (4 << 2)),
		-1,	/* IMPLICIT tag at current level */
		&asn_DEF_RoutingAdvertisement,
		0,	/* Defer constraints checking to the member type */
		0,	/* No PER visible constraints */
		0,
		"routingAdvertisement"
		},
};
static const int asn_MAP_SrvAdvBody_oms_1[] = { 1, 2, 3, 4 };
static const ber_tlv_tag_t asn_DEF_SrvAdvBody_tags_1[] = {
	(ASN_TAG_CLASS_UNIVERSAL | (16 << 2))
};
static const asn_TYPE_tag2member_t asn_MAP_SrvAdvBody_tag2el_1[] = {
    { (ASN_TAG_CLASS_CONTEXT | (0 << 2)), 0, 0, 0 }, /* changeCount */
    { (ASN_TAG_CLASS_CONTEXT | (1 << 2)), 1, 0, 0 }, /* extensions */
    { (ASN_TAG_CLASS_CONTEXT | (2 << 2)), 2, 0, 0 }, /* serviceInfos */
    { (ASN_TAG_CLASS_CONTEXT | (3 << 2)), 3, 0, 0 }, /* channelInfos */
    { (ASN_TAG_CLASS_CONTEXT | (4 << 2)), 4, 0, 0 } /* routingAdvertisement */
};
static asn_SEQUENCE_specifics_t asn_SPC_SrvAdvBody_specs_1 = {
	sizeof(struct SrvAdvBody),
	offsetof(struct SrvAdvBody, _asn_ctx),
	asn_MAP_SrvAdvBody_tag2el_1,
	5,	/* Count of tags in the map */
	asn_MAP_SrvAdvBody_oms_1,	/* Optional members */
	4, 0,	/* Root/Additions */
	-1,	/* Start extensions */
	-1	/* Stop extensions */
};
asn_TYPE_descriptor_t asn_DEF_SrvAdvBody = {
	"SrvAdvBody",
	"SrvAdvBody",
	SEQUENCE_free,
	SEQUENCE_print,
	SEQUENCE_constraint,
	SEQUENCE_decode_ber,
	SEQUENCE_encode_der,
	SEQUENCE_decode_xer,
	SEQUENCE_encode_xer,
	SEQUENCE_decode_uper,
	SEQUENCE_encode_uper,
	0,	/* Use generic outmost tag fetcher */
	asn_DEF_SrvAdvBody_tags_1,
	sizeof(asn_DEF_SrvAdvBody_tags_1)
		/sizeof(asn_DEF_SrvAdvBody_tags_1[0]), /* 1 */
	asn_DEF_SrvAdvBody_tags_1,	/* Same as above */
	sizeof(asn_DEF_SrvAdvBody_tags_1)
		/sizeof(asn_DEF_SrvAdvBody_tags_1[0]), /* 1 */
	0,	/* No PER visible constraints */
	asn_MBR_SrvAdvBody_1,
	5,	/* Elements count */
	&asn_SPC_SrvAdvBody_specs_1	/* Additional specs */
};

