/*
 * Generated by asn1c-0.9.29 (http://lionet.info/asn1c)
 * From ASN.1 module "Ieee1609Dot3Wee"
 * 	found in "../Ieee1609Dot3Wee.asn"
 * 	`asn1c -fcompound-names -gen-PER`
 */

#ifndef	_Extension_H_
#define	_Extension_H_


#include <asn_application.h>

/* Including external dependencies */
#include "RefExt.h"
#include <ANY.h>
#include <constr_SEQUENCE.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Extension */
typedef struct Extension_22P0 {
	RefExt_t	 extensionId;
	ANY_t	 value;
	
	/* Context for parsing across buffer boundaries */
	asn_struct_ctx_t _asn_ctx;
} Extension_22P0_t;
typedef struct Extension_22P1 {
	RefExt_t	 extensionId;
	ANY_t	 value;
	
	/* Context for parsing across buffer boundaries */
	asn_struct_ctx_t _asn_ctx;
} Extension_22P1_t;
typedef struct Extension_22P2 {
	RefExt_t	 extensionId;
	ANY_t	 value;
	
	/* Context for parsing across buffer boundaries */
	asn_struct_ctx_t _asn_ctx;
} Extension_22P2_t;
typedef struct Extension_22P3 {
	RefExt_t	 extensionId;
	ANY_t	 value;
	
	/* Context for parsing across buffer boundaries */
	asn_struct_ctx_t _asn_ctx;
} Extension_22P3_t;

/* Implementation */
extern asn_TYPE_descriptor_t asn_DEF_Extension_22P0;
extern asn_TYPE_descriptor_t asn_DEF_Extension_22P1;
extern asn_TYPE_descriptor_t asn_DEF_Extension_22P2;
extern asn_TYPE_descriptor_t asn_DEF_Extension_22P3;

#ifdef __cplusplus
}
#endif

#endif	/* _Extension_H_ */
#include <asn_internal.h>
