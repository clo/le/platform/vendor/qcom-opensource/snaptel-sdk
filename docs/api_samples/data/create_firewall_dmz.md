Create Firewall DMZ {#create_firewall_dmz}
==========================================

# Create Firewall DMZ

Please follow below steps to create firewall DMZ

### 1. Implement initialization callback and get the DataFactory instances ###
Optionally initialization callback can be provided with get manager instance.
Data factory will call callback when manager initialization is complete.

   ~~~~~~{.cpp}
   auto initCb = [&](telux::common::ServiceStatus status) {
      std::lock_guard<std::mutex> lock(mtx);
      status_ = status;
      initCv.notify_all();
   };
   auto &dataFactory = telux::data::DataFactory::getInstance();
   ~~~~~~

### 2. Get the FirewallManager instances

   ~~~~~~{.cpp}
   std::unique_lock<std::mutex> lck(mtx);
   auto dataFwMgr  = dataFactory.getFirewallManager(opType, initCb);
   ~~~~~~

### 3. Wait for FirewallManager initialization to be complete

   ~~~~~~{.cpp}
   initCv.wait(lck);
   ~~~~~~

### 3.1 Check FirewallManager initialization state

If FirewallManager initialization failed, new initialization attempt can be accomplished
by calling step 2. If FirewallManager initialization succeed, proceed to step 4

   ~~~~~~{.cpp}
   if (status_ == telux::common::ServiceStatus::SERVICE_AVAILABLE) {
      // Go to step 4
   }
   else {
      //Go to step 2 for another initialization attempt
   }
   ~~~~~~

### 4. Implement callback for create DMZ ###

   ~~~~~~{.cpp}
   auto respCb = [](telux::common::ErrorCode error) {
      std::cout << std::endl << std::endl;
      std::cout << "CALLBACK: "
                  << "addDmz Response"
                  << (error == telux::common::ErrorCode::SUCCESS ? " is successful" : " failed");
   };
   ~~~~~~

### 5. Create DMZ based on profile id and local ip address ###

   ~~~~~~{.cpp}
   dataFwMgr->enableDmz(profileId,ipAddr, respCb);
   ~~~~~~

### 6. Response callback will be called for the addDmz response ###
