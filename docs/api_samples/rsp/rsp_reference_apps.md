Using Remote SIM Provisioning Reference Service {#rsp_reference_apps}
============================================================

# Using Remote SIM Provisioning Reference Service

SIM profile management operations such as add profile, delete profile, enable/disable profile
requires HTTP interaction with the cloud to synchronize the profile/s on the eUICC with the
SMDP+/SMDS server. When modem LPA/eUICC needs to reach SMDP+/SMDS server on the cloud for HTTP
transaction, the HTTP request is sent to Remote SIM provisioning service i.e RSP HTTP daemon running
on Application Processor. The RSP HTTP Daemon performs these HTTP transactions on behalf of modem
with SMDP+/SMDS server running on the cloud. The HTTP response from cloud is sent back to modem
LPA/eUICC to take appropriate action. Make sure there is internet connectivity available for
performing HTTP transaction either using WLAN, WWAN or ethernet.

This section describes how to use the provided Remote SIM provisioning reference service – rsp_httpd

### 1. Run the Remote SIM HTTP Daemon on the device

~~~~~~
rsp_httpd
~~~~~~

Use -h option to check for all the options supported and usage instructions.

