/*
 * Generated by asn1c-0.9.29 (http://lionet.info/asn1c)
 * From ASN.1 module "IEEE-1609-3-WSA"
 * 	found in "../Ieee1609Dot3Wsa.asn"
 * 	`asn1c -fcompound-names -gen-PER`
 */

#include "ChannelInfo.h"

static asn_TYPE_member_t asn_MBR_ChannelInfo_1[] = {
	{ ATF_NOFLAGS, 0, offsetof(struct ChannelInfo, operatingClass),
		(ASN_TAG_CLASS_CONTEXT | (0 << 2)),
		-1,	/* IMPLICIT tag at current level */
		&asn_DEF_OperatingClass80211,
		0,	/* Defer constraints checking to the member type */
		0,	/* No PER visible constraints */
		0,
		"operatingClass"
		},
	{ ATF_NOFLAGS, 0, offsetof(struct ChannelInfo, channelNumber),
		(ASN_TAG_CLASS_CONTEXT | (1 << 2)),
		-1,	/* IMPLICIT tag at current level */
		&asn_DEF_ChannelNumber80211,
		0,	/* Defer constraints checking to the member type */
		0,	/* No PER visible constraints */
		0,
		"channelNumber"
		},
	{ ATF_NOFLAGS, 0, offsetof(struct ChannelInfo, powerLevel),
		(ASN_TAG_CLASS_CONTEXT | (2 << 2)),
		-1,	/* IMPLICIT tag at current level */
		&asn_DEF_TXpower80211,
		0,	/* Defer constraints checking to the member type */
		0,	/* No PER visible constraints */
		0,
		"powerLevel"
		},
	{ ATF_NOFLAGS, 0, offsetof(struct ChannelInfo, dataRate),
		(ASN_TAG_CLASS_CONTEXT | (3 << 2)),
		-1,	/* IMPLICIT tag at current level */
		&asn_DEF_WsaChInfoDataRate,
		0,	/* Defer constraints checking to the member type */
		0,	/* No PER visible constraints */
		0,
		"dataRate"
		},
	{ ATF_NOFLAGS, 0, offsetof(struct ChannelInfo, extensions),
		(ASN_TAG_CLASS_CONTEXT | (4 << 2)),
		-1,	/* IMPLICIT tag at current level */
		&asn_DEF_ChInfoOptions,
		0,	/* Defer constraints checking to the member type */
		0,	/* No PER visible constraints */
		0,
		"extensions"
		},
};
static const ber_tlv_tag_t asn_DEF_ChannelInfo_tags_1[] = {
	(ASN_TAG_CLASS_UNIVERSAL | (16 << 2))
};
static const asn_TYPE_tag2member_t asn_MAP_ChannelInfo_tag2el_1[] = {
    { (ASN_TAG_CLASS_CONTEXT | (0 << 2)), 0, 0, 0 }, /* operatingClass */
    { (ASN_TAG_CLASS_CONTEXT | (1 << 2)), 1, 0, 0 }, /* channelNumber */
    { (ASN_TAG_CLASS_CONTEXT | (2 << 2)), 2, 0, 0 }, /* powerLevel */
    { (ASN_TAG_CLASS_CONTEXT | (3 << 2)), 3, 0, 0 }, /* dataRate */
    { (ASN_TAG_CLASS_CONTEXT | (4 << 2)), 4, 0, 0 } /* extensions */
};
static asn_SEQUENCE_specifics_t asn_SPC_ChannelInfo_specs_1 = {
	sizeof(struct ChannelInfo),
	offsetof(struct ChannelInfo, _asn_ctx),
	asn_MAP_ChannelInfo_tag2el_1,
	5,	/* Count of tags in the map */
	0, 0, 0,	/* Optional elements (not needed) */
	-1,	/* Start extensions */
	-1	/* Stop extensions */
};
asn_TYPE_descriptor_t asn_DEF_ChannelInfo = {
	"ChannelInfo",
	"ChannelInfo",
	SEQUENCE_free,
	SEQUENCE_print,
	SEQUENCE_constraint,
	SEQUENCE_decode_ber,
	SEQUENCE_encode_der,
	SEQUENCE_decode_xer,
	SEQUENCE_encode_xer,
	SEQUENCE_decode_uper,
	SEQUENCE_encode_uper,
	0,	/* Use generic outmost tag fetcher */
	asn_DEF_ChannelInfo_tags_1,
	sizeof(asn_DEF_ChannelInfo_tags_1)
		/sizeof(asn_DEF_ChannelInfo_tags_1[0]), /* 1 */
	asn_DEF_ChannelInfo_tags_1,	/* Same as above */
	sizeof(asn_DEF_ChannelInfo_tags_1)
		/sizeof(asn_DEF_ChannelInfo_tags_1[0]), /* 1 */
	0,	/* No PER visible constraints */
	asn_MBR_ChannelInfo_1,
	5,	/* Elements count */
	&asn_SPC_ChannelInfo_specs_1	/* Additional specs */
};

