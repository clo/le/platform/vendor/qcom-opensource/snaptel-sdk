/*
 *  Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 *  ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 *  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef VOICEMENU_HPP
#define VOICEMENU_HPP

#include <map>

#include "ConsoleApp.hpp"
#include "../../common/Audio/VoiceSession.hpp"
#include "../../common/Audio/AudioHelper.hpp"
#include <telux/audio/AudioListener.hpp>

class VoiceMenu : public ConsoleApp,
                  public telux::audio::IVoiceListener,
                  public std::enable_shared_from_this<VoiceMenu>{
public:
    VoiceMenu(std::string appName, std::string cursor);
    ~VoiceMenu();
    void init();
    void setSystemReady();
    void cleanup();
    virtual void onDtmfToneDetection(DtmfTone dtmfTone) override;

private:
    void createStream(std::vector<std::string> userInput);
    void deleteStream(std::vector<std::string> userInput);
    void getDevice(std::vector<std::string> userInput);
    void setDevice(std::vector<std::string> userInput);
    void getVolume(std::vector<std::string> userInput);
    void setVolume(std::vector<std::string> userInput);
    void getMute(std::vector<std::string> userInput);
    void setMute(std::vector<std::string> userInput);
    void startAudio(std::vector<std::string> userInput);
    void stopAudio(std::vector<std::string> userInput);
    void startDtmf(std::vector<std::string> userInput);
    void stopDtmf(std::vector<std::string> userInput);
    void registerListener(std::vector<std::string> userInput);
    void deRegisterListener(std::vector<std::string> userInput);
    void changeSlotId();
    void setActiveSession(SlotId slotId);

    std::shared_ptr<VoiceSession> activeSession_;
    std::mutex mutex_;
    std::map<SlotId, std::shared_ptr<VoiceSession>> voiceSessions_;
    std::atomic<bool> ready_;
    SlotId slotId_;
};

#endif // VOICEMENU_HPP
