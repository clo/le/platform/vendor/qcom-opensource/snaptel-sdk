/*
 *  Copyright (c) 2019, The Linux Foundation. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 *  ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 *  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef THERMALSHUTDOWNTEST_HPP
#define THERMALSHUTDOWNTEST_HPP

#include <memory>
#include <getopt.h>
#include <mutex>
#include <condition_variable>

#include <telux/therm/ThermalDefines.hpp>
#include <telux/therm/ThermalFactory.hpp>
#include <telux/therm/ThermalShutdownManager.hpp>
#include <telux/therm/ThermalShutdownListener.hpp>
#include "ConsoleApp.hpp"

#include "ThermalCommandMgr.hpp"

#define APP_NAME "\033[1;32mThermal_shutdown_test_app\033[0m"

using namespace telux::therm;
using namespace telux::common;

class ThermalShutdownTestApp : public IThermalShutdownListener,
                         public ConsoleApp {
public:

    ~ThermalShutdownTestApp();

    std::mutex mtx_;
    std::condition_variable cv_;

    static ThermalShutdownTestApp & getInstance();
    //static std::shared_ptr<ThermalShutdownTestApp> & getInstance();
    int init();

    void printHelp();
    void signalHandler( int signum );
    Status parseArguments(int argc, char **argv);
    void handleArguments();
    void consoleinit();
    bool listnerEnableStatus();

private:
    ThermalShutdownTestApp();
    // Member variable to keep the command manager object alive till application ends.
    std::shared_ptr<ThermalCommandMgr> myThermCmdMgr_;
    bool exiting_;
    bool listenerEnabled_;
    AutoShutdownMode setCommand_;
    bool getCommand_;
    bool isConsole_;

};

#endif  // THERMALSHUTDOWNTEST_HPP
