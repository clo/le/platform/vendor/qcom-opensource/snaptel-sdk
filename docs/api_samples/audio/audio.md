audio {#audio}
==============

# The List of sample apps related to audio

* @subpage audio_manager_api
* @subpage audio_manager_playback
* @subpage audio_manager_capture
* @subpage audio_manager_loopback
* @subpage audio_manager_tonegenerator
* @subpage audio_manager_voicecall_start_stop
* @subpage audio_manager_voicecall_volume_mute
* @subpage audio_audio_manager_voicecall_device_switch
* @subpage audio_voicecall_dtmf_play
* @subpage audio_voicecall_dtmf_detect
* @subpage audio_transcoding_operation
* @subpage compressed_audio_format_playback
* @subpage compressed_audio_format_playback_on_voice_paths
